package com.example.apka1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Zmienne przycisków
    Button zadzwon, otworzStrone, zdjecie, gmaps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        zadzwon = (Button) findViewById(R.id.zadzwon);
        otworzStrone = (Button) findViewById(R.id.otworzStrone);
        zdjecie = (Button) findViewById(R.id.zdjecie);
        gmaps = (Button) findViewById(R.id.gmaps);

        gmaps.setOnClickListener(this);
        zadzwon.setOnClickListener(this);
        otworzStrone.setOnClickListener(this);
        zdjecie.setOnClickListener(this);


    }

//Przypisanie funckji przyciskom
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            //przycisk przenoszący do telefonu
            case R.id.zadzwon:
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"));
            startActivity(intent);
            break;
            //przycisk przenoszący do przeglądarki
            case R.id.otworzStrone:
                Intent intent2 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/"));
                startActivity(intent2);
                break;
            //przycisk uruchamiający aktywność "zdjecie"
            case R.id.zdjecie:
            Intent intent3 = new Intent(MainActivity.this, zdjecie.class);
            startActivity(intent3);
            break;
            //przycisk do aktywności gmaps
           case R.id.gmaps:
                Intent intent5 = new Intent(MainActivity.this, gmaps.class);
                startActivity(intent5);
                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("tag", "In onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("tag", "In onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("tag", "In onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("tag", "In onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("tag", "In onDestroy");
    }

}
